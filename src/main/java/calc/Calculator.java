package calc;

public class Calculator {

    public static int sum(int a, int b) {
        return a + b;
    }

    public static double divide(int a, int b) {
        return (double)a / b;
    }

}
