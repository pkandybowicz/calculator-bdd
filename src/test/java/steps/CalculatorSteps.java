package steps;

import calc.Calculator;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;

public class CalculatorSteps {

    double result;

    @When("I sum {int} and {int}")
    public void i_sum_and(Integer int1, Integer int2) {
        result = Calculator.sum(int1, int2);
    }

    @Then("I get result value {double}")
    public void i_get_result_value(Double int1) {
        Assertions.assertEquals(int1, result);
    }

    @When("I divide {int} and {int}")
    public void i_divide_and(Integer a, Integer b) {
        result= Calculator.divide(a, b);
    }

}
