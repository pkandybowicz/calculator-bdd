Feature: Calculator
  calculator should add and divide 2 numbers

  Scenario: Sum two positive numbers
    When I sum 2 and 3
    Then I get result value 5

  Scenario: Divide two positive numbers
    When I divide 5 and 2
    Then I get result value 2.5

  Scenario Outline: Divide different values
    When I divide <number1> and <number2>
    Then I get result value <result>

    Examples:
      | number1 | number2 | result |
      | 10      | 2       |5       |
      | 12      | 6       |2       |
      | -10     | 4       | -2.5   |
